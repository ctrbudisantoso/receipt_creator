unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, receipt,fpjsonrtti, fpjson;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Memo1: TMemo;
    SelectDirectoryDialog1: TSelectDirectoryDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;
  //JsonNode1: TJSON

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin

  if SelectDirectoryDialog1.Execute then
     Edit1.Text:=SelectDirectoryDialog1.FileName;

end;

procedure TForm1.Button2Click(Sender: TObject);
var
  rec:TReceipt;
  jsoSerialize: TJSONStreamer;
begin
  jsoSerialize := TJSONStreamer.Create(nil);
  rec := TReceipt.create;
  rec.Devid := 'test';
  Edit2.Text := rec.Devid;
  try
    //
    Memo1.Text := jsoSerialize.ObjectToJSONString(rec);
  finally
    jsoSerialize.Free;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

end;

end.

